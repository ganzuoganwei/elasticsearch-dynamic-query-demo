package com.example.elasticserachdynamicquerydemo.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;
import java.util.Objects;

/**
 * 债券基础信息源配置
 *
 * @author Frank
 * @date 2020/02/27
 **/
@Configuration
@MapperScan(basePackages = {"com.example.elasticserachdynamicquerydemo.mapper"},
        sqlSessionFactoryRef = DataSourceConfig.SESSION_FACTORY_NAME)
public class DataSourceConfig {
    public static final String TRANSACTION_NAME = "dataSourceTransactionManager";

    public static final String DATA_SOURCE_NAME = "datasource";
    public static final String DATA_SOURCE_PREFIX = "datasource";
    public static final String SESSION_FACTORY_NAME = "sqlSessionFactory";
    protected static final String[] ALIAS_PACKAGES = {"com.example.elasticserachdynamicquerydemo.modle.entity"};

    /**
     * 创建数据源
     *
     * @return 返回数据源
     */
    @Bean(name = DATA_SOURCE_NAME, initMethod = "init", destroyMethod = "close")
    @ConfigurationProperties(prefix = DATA_SOURCE_PREFIX)
    public DruidDataSource dataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 配置事务
     *
     * @return 事务
     */
    @Bean(name = TRANSACTION_NAME)
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    /**
     * 创建SqlSessionFactory对象
     *
     * @param dataSource 数据源
     * @return SqlSessionFactory对象
     * @throws Exception 异常
     */
    @Bean(name = SESSION_FACTORY_NAME)
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Qualifier(DATA_SOURCE_NAME) DataSource dataSource) throws Exception {
        return getSessionFactory(dataSource, ALIAS_PACKAGES);
    }

    SqlSessionFactory getSessionFactory(DataSource dataSource, String[] aliasPackages) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();

        sessionFactory.setDataSource(dataSource);
        sessionFactory.setTypeAliasesPackage(String.join(",", aliasPackages));
        SqlSessionFactory factory = sessionFactory.getObject();
        if (Objects.nonNull(factory)) {
            factory.getConfiguration().setMapUnderscoreToCamelCase(true);
        }
        return factory;
    }
}
