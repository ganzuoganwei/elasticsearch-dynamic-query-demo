package com.example.elasticserachdynamicquerydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * springboot 入口
 */
@EnableSwagger2
@SpringBootApplication
public class ElasticsearchDynamicQueryDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchDynamicQueryDemoApplication.class, args);
    }

}
