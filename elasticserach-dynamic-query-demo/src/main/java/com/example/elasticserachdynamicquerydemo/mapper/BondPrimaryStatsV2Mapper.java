package com.example.elasticserachdynamicquerydemo.mapper;

import com.example.elasticserachdynamicquerydemo.model.entity.BondPrimaryStatsV2DO;
import com.github.wz2cool.dynamic.mybatis.mapper.DynamicQueryMapper;

/**
 * 一级发行统计Mapper
 *
 * @author xionglei
 * @date 2022/07/15 14:03
 */
public interface BondPrimaryStatsV2Mapper extends DynamicQueryMapper<BondPrimaryStatsV2DO> {

}
