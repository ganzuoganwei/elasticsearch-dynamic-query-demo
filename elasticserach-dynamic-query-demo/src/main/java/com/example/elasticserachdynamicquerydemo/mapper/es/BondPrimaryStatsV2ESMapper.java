package com.example.elasticserachdynamicquerydemo.mapper.es;

import com.example.elasticserachdynamicquerydemo.model.es.BondPrimaryStatsV2ES;
import com.github.wz2cool.elasticsearch.repository.ElasticsearchExtRepository;
import org.springframework.data.elasticsearch.annotations.Query;

import java.util.List;

/**
 * mapper 文件
 *
 * @author xionglei
 * @date 2022/7/26 19:22
 **/
public interface BondPrimaryStatsV2ESMapper extends ElasticsearchExtRepository<BondPrimaryStatsV2ES, Long> {

    @Query("{\"term\":{\"issueDateMonth\":{\"value\":\"201503\"}}}")
    List<BondPrimaryStatsV2ES> listByQuery();

}
