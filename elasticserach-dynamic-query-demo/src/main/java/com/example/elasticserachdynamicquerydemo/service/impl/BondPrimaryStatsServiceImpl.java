package com.example.elasticserachdynamicquerydemo.service.impl;

import com.example.elasticserachdynamicquerydemo.dao.BondPrimaryStatsV2DAO;
import com.example.elasticserachdynamicquerydemo.dao.es.BondPrimaryStatsV2ESDAO;
import com.example.elasticserachdynamicquerydemo.model.dto.BondPrimaryStatsResponseDTO;
import com.example.elasticserachdynamicquerydemo.model.entity.BondPrimaryStatsV2DO;
import com.example.elasticserachdynamicquerydemo.model.es.BondPrimaryStatsV2ES;
import com.example.elasticserachdynamicquerydemo.service.BondPrimaryStatsService;
import com.example.elasticserachdynamicquerydemo.utils.BeanCopyUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * 一级统计服务
 *
 * @author xionglei
 * @date 2022/7/29 18:49
 **/
@Service
public class BondPrimaryStatsServiceImpl implements BondPrimaryStatsService {

    private static final int FETCH_COUNT = 100;

    @Resource
    private BondPrimaryStatsV2DAO bondPrimaryStatsV2DAO;
    @Resource
    private BondPrimaryStatsV2ESDAO bondPrimaryStatsV2ESDAO;

    @Override
    public void refreshBondPrimaryStatsES() {
        Integer fetchCount = null;
        long startId = 0L;
        while (Objects.isNull(fetchCount) || fetchCount == FETCH_COUNT) {
            List<BondPrimaryStatsV2DO> bondPrimaryStatsV2DOS = bondPrimaryStatsV2DAO.listBondPrimaryStats(startId, FETCH_COUNT);
            fetchCount = bondPrimaryStatsV2DOS.size();
            List<BondPrimaryStatsV2ES> bondPrimaryStatsV2ES = BeanCopyUtils.copyList(bondPrimaryStatsV2DOS, BondPrimaryStatsV2ES.class);
            bondPrimaryStatsV2ESDAO.save(bondPrimaryStatsV2ES);
            startId = bondPrimaryStatsV2DOS.stream().map(BondPrimaryStatsV2DO::getId).max(Long::compareTo).orElse(startId);
        }

    }

    @Override
    public List<BondPrimaryStatsResponseDTO> listBondPrimaryStats(Date startDate, Date endDate) {
        return bondPrimaryStatsV2ESDAO.listBondPrimaryStats(startDate, endDate);
    }
}
