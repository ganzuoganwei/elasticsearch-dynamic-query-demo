package com.example.elasticserachdynamicquerydemo.service;

import com.example.elasticserachdynamicquerydemo.model.dto.BondPrimaryStatsResponseDTO;

import java.sql.Date;
import java.util.List;

/**
 * 描述
 *
 * @author xionglei
 * @date 2022/7/29 18:42
 **/
public interface BondPrimaryStatsService {

    /**
     * 刷新一级统计数据
     */
    void refreshBondPrimaryStatsES();

    /**
     * 查询数据
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 数据返回
     */
    List<BondPrimaryStatsResponseDTO> listBondPrimaryStats(Date startDate, Date endDate);

}
