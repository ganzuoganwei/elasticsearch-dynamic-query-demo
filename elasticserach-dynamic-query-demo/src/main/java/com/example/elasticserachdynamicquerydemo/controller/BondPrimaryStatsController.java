package com.example.elasticserachdynamicquerydemo.controller;

import com.example.elasticserachdynamicquerydemo.model.dto.BondPrimaryStatsResponseDTO;
import com.example.elasticserachdynamicquerydemo.service.BondPrimaryStatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;

/**
 * 描述
 *
 * @author xionglei
 * @date 2022/7/29 18:57
 **/
@Api(tags = "(内部)一级发行统计接口")
@RestController
@RequestMapping("bond/primary/stats")
public class BondPrimaryStatsController {

    @Resource
    private BondPrimaryStatsService bondPrimaryStatsService;

    @ApiOperation("刷新一级发行统计数据")
    @GetMapping("/refresh")
    public void refreshBondPrimaryStatsES() {
        bondPrimaryStatsService.refreshBondPrimaryStatsES();
    }

    @ApiOperation("分组统计数据")
    @GetMapping("/search")
    public List<BondPrimaryStatsResponseDTO> listBondPrimaryStats(@RequestParam Date startDate, @RequestParam Date endDate) {
        return bondPrimaryStatsService.listBondPrimaryStats(startDate, endDate);
    }
}
