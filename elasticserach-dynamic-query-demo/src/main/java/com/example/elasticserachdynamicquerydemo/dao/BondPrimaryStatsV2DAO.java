package com.example.elasticserachdynamicquerydemo.dao;

import com.example.elasticserachdynamicquerydemo.mapper.BondPrimaryStatsV2Mapper;
import com.example.elasticserachdynamicquerydemo.model.dto.BondPrimaryStatsResponseDTO;
import com.example.elasticserachdynamicquerydemo.model.entity.BondPrimaryStatsV2DO;
import com.example.elasticserachdynamicquerydemo.model.es.BondPrimaryStatsV2ES;
import com.github.wz2cool.dynamic.DynamicQuery;
import com.github.wz2cool.dynamic.SortDirections;
import com.github.wz2cool.dynamic.builder.DynamicQueryBuilderHelper;
import com.google.common.collect.Lists;
import org.apache.ibatis.session.RowBounds;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ParsedAvg;
import org.elasticsearch.search.aggregations.metrics.ParsedSum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 一级发行统计
 *
 * @Author caodz
 * @Date 2022/7/15
 **/
@Repository
public class BondPrimaryStatsV2DAO {

    @Resource
    private BondPrimaryStatsV2Mapper bondPrimaryStatsV2Mapper;

    /**
     * 获取数据
     *
     * @param startId    开始id
     * @param fetchCount 获取数量
     * @return 一级统计数据
     */
    public List<BondPrimaryStatsV2DO> listBondPrimaryStats(Long startId, Integer fetchCount) {
        DynamicQuery<BondPrimaryStatsV2DO> dynamicQuery = DynamicQuery.createQuery(BondPrimaryStatsV2DO.class)
                .and(BondPrimaryStatsV2DO::getId, DynamicQueryBuilderHelper.greaterThan(startId))
                .orderBy(BondPrimaryStatsV2DO::getId, SortDirections::asc);
        return bondPrimaryStatsV2Mapper.selectRowBoundsByDynamicQuery(dynamicQuery, new RowBounds(0, fetchCount));
    }
}
