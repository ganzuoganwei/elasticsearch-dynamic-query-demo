package com.example.elasticserachdynamicquerydemo.dao.es;

import com.example.elasticserachdynamicquerydemo.mapper.es.BondPrimaryStatsV2ESMapper;
import com.example.elasticserachdynamicquerydemo.model.dto.BondPrimaryStatsResponseDTO;
import com.example.elasticserachdynamicquerydemo.model.es.BondPrimaryStatsV2ES;
import com.google.common.collect.Lists;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ParsedAvg;
import org.elasticsearch.search.aggregations.metrics.ParsedSum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * es dao 层数据
 *
 * @author xionglei
 * @date 2022/7/26 19:23
 **/
@Repository
public class BondPrimaryStatsV2ESDAO {

    @Resource
    private BondPrimaryStatsV2ESMapper bondPrimaryStatsV2ESMapper;
    @Resource
    private ElasticsearchOperations elasticsearchOperations;

    /**
     * 批量保存es数据
     *
     * @param bondPrimaryStatsV2ESList es 数据列表
     */
    public void save(Collection<BondPrimaryStatsV2ES> bondPrimaryStatsV2ESList) {
        bondPrimaryStatsV2ESMapper.saveAll(bondPrimaryStatsV2ESList);
    }

    /**
     * 根据省份统计发行规模数据
     *
     * @return 发行规模返回值
     */
    public List<BondPrimaryStatsResponseDTO> listBondPrimaryStats(Date startDate, Date endDate) {
        TermsAggregationBuilder termsAggregationBuilder =
                AggregationBuilders.terms("provinceBucket").field("provinceUniCode")
                        .size(100)
                        .subAggregation(AggregationBuilders.sum("actualAmtSum").field("actualIssueAmount"))
                        .subAggregation(AggregationBuilders.avg("issueCouponAvg").field("issueCoupon"));
        RangeQueryBuilder issueDate = QueryBuilders.rangeQuery("issueDate").gte(startDate.toString()).lte(endDate.toString());
        NativeSearchQueryBuilder query =
                new NativeSearchQueryBuilder()
                        .withQuery(issueDate)
                        .addAggregation(termsAggregationBuilder).withPageable(Pageable.ofSize(1));
        SearchHits<BondPrimaryStatsV2ES> searchHits = elasticsearchOperations.search(query.build(), BondPrimaryStatsV2ES.class);
        Map<String, Aggregation> stringAggregationMap = new HashMap<>();
        if (searchHits.getAggregations() != null && searchHits.getAggregations().getAsMap() != null) {
            stringAggregationMap = searchHits.getAggregations().asMap();

        }
        ParsedLongTerms terms = (ParsedLongTerms) stringAggregationMap.get("provinceBucket");
        List<? extends Terms.Bucket> buckets = terms.getBuckets();
        List<BondPrimaryStatsResponseDTO> resultList = Lists.newArrayList();
        for (Terms.Bucket bucket : buckets) {
            BondPrimaryStatsResponseDTO responseDTO = new BondPrimaryStatsResponseDTO();
            responseDTO.setCode(Long.valueOf(bucket.getKeyAsString()));
            responseDTO.setIssueCount(bucket.getDocCount());
            Aggregations aggregations = bucket.getAggregations();
            ParsedSum actualAmtSum = aggregations.get("actualAmtSum");
            ParsedAvg issueCouponAvg = aggregations.get("issueCouponAvg");
            if (!Double.valueOf(actualAmtSum.getValue()).isInfinite()) {
                BigDecimal actualAmtSumValue = BigDecimal.valueOf(actualAmtSum.getValue());
                responseDTO.setIssueAmount(actualAmtSumValue);
            }
            if (!Double.valueOf(issueCouponAvg.getValue()).isInfinite()) {
                BigDecimal issueCouponAvgValue = BigDecimal.valueOf(issueCouponAvg.getValue());
                responseDTO.setAvgCouponRate(issueCouponAvgValue);
            }
            resultList.add(responseDTO);
        }
        return resultList;
    }
}
