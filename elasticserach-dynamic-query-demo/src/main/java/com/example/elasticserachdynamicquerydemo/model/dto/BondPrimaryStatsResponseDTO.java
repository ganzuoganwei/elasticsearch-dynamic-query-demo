package com.example.elasticserachdynamicquerydemo.model.dto;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * 描述
 *
 * @author xionglei
 * @date 2022/8/3 15:13
 **/
public class BondPrimaryStatsResponseDTO {

    @ApiModelProperty("数据Code")
    private Long code;
    @ApiModelProperty("发行规模")
    private BigDecimal issueAmount;
    @ApiModelProperty("发行数量")
    private Long issueCount;
    @ApiModelProperty("平均票息")
    private BigDecimal avgCouponRate;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BigDecimal getIssueAmount() {
        return issueAmount;
    }

    public void setIssueAmount(BigDecimal issueAmount) {
        this.issueAmount = issueAmount;
    }

    public Long getIssueCount() {
        return issueCount;
    }

    public void setIssueCount(Long issueCount) {
        this.issueCount = issueCount;
    }

    public BigDecimal getAvgCouponRate() {
        return avgCouponRate;
    }

    public void setAvgCouponRate(BigDecimal avgCouponRate) {
        this.avgCouponRate = avgCouponRate;
    }
}
