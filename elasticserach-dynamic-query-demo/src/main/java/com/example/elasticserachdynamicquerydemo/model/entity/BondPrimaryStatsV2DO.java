package com.example.elasticserachdynamicquerydemo.model.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * 一级统计字段v2版本DO
 */
@Table(name = "bond_primary_stats_v2")
public class BondPrimaryStatsV2DO {
    /**
     * 主键id
     */
    @Id
    @Column
    private Long id;
    /**
     * 创建时间
     */
    @Column
    private Timestamp createTime;
    /**
     * 更新时间
     */
    @Column
    private Timestamp updateTime;
    /**
     * 发行日期: 根据债券的状态进行判断处理 issue_status: (0,1) 取的是：t_bond_basic_info 表里面的： iss_end_date; (2,3)取的是：iss_decl_date
     */
    @Column
    private Date issueDate;
    /**
     * 发行日期-周一, 对应issue_date的周一
     */
    @Column
    private Date issueDateMonday;
    /**
     * 发行日期-月
     */
    @Column
    private Integer issueDateMonth;
    /**
     * 发行日期-季: 组成方式： 年 + 季 (01:1 季度; 02: 2季度; 03: 3季度; 04: 4季度)
     */
    @Column
    private Integer issueDateQuarter;
    /**
     * 发行日期-年
     */
    @Column
    private Integer issueDateYear;
    /**
     * 到期日期
     */
    @Column
    private Date maturityDate;
    /**
     * 到期日期-月
     */
    @Column
    private Date maturityDateMonday;
    /**
     * 发行日期-月
     */
    @Column
    private Integer maturityDateMonth;
    /**
     * 发行日期-季: 组成方式： 年 + 季 (01:1 季度; 02: 2季度; 03: 3季度; 04: 4季度)
     */
    @Column
    private Integer maturityDateQuarter;
    /**
     * 发行日期-年
     */
    @Column
    private Integer maturityDateYear;
    /**
     * 债券唯一编码
     */
    @Column
    private Long bondUniCode;
    /**
     * 债券简称
     */
    @Column
    private String bondShortName;
    /**
     * 发行人唯一编码
     */
    @Column
    private Long comUniCode;
    /**
     * 发行人全称
     */
    @Column
    private String comFullName;
    /**
     * 债券类型
     */
    @Column
    private Integer bondType;
    /**
     * 交易场所： 1 深圳证券交易所;2 上海证券交易所; 3  银行间市场;4  柜台交易市场; 99 其他
     */
    @Column
    private Integer secondMarket;
    /**
     * 募集方式： 0： 私募； 1：公募
     */
    @Column
    private Integer publicOffering;
    /**
     * 发行条款： 0:含权 1：不含权 2:永续
     */
    @Column
    private Integer embeddedOption;
    /**
     * 票面利率类型：1：固定利率2：浮动利率3：累进利率 4：贴现 5：无序利率 99： 其他
     */
    @Column
    private Integer couponRateType;
    /**
     * 债券期限: 天数
     */
    @Column
    private Integer bondTenorDay;
    /**
     * 跨市场： 0 否； 1：是
     */
    @Column
    private Integer crossMarketStatus;
    /**
     * 主体外部评级
     */
    @Column
    private Integer comExtRatingMapping;
    /**
     * 债券外部评级
     */
    @Column
    private Integer bondExtRatingMapping;
    /**
     * 发行人是否上市： 0: 否； 1： 是
     */
    @Column
    private Integer comListedStatus;
    /**
     * 企业性质：详细枚举值见字典
     */
    @Column
    private Integer businessNature;
    /**
     * 是否城投：0：否； 1： 是
     */
    @Column
    private Integer udicStatus;
    /**
     * 发行票面利率
     */
    @Column
    private BigDecimal issueCouponRate;
    /**
     * 债券发行状态：0:  发行中; 1:  已经上市; 2:  延迟发行; 3:  取消发行
     */
    @Column
    private Integer issueStatus;
    /**
     * 实际发行金额/
     */
    @Column
    private BigDecimal actualIssueAmount;
    /**
     * 发行票息 = 实际发行金额 * 票面利率
     */
    @Column
    private BigDecimal issueCoupon;
    /**
     * 省份唯一编码 1 城投, 这个字段取的是城投省份 2 非城投, 这个字段取的是注册地省份
     */
    @Column
    private Long provinceUniCode;
    /**
     * 省份名称
     */
    @Column
    private String provinceName;
    /**
     * 地级市唯一编码 1 城投, 这个字段取的是城投地级市唯一编码 2 非城投, 这个字段取的是注册地地级市唯一编码
     */
    @Column
    private Long cityUniCode;
    /**
     * 城市名称
     */
    @Column
    private Long cityName;
    /**
     * 区县编码 1 城投, 取得是城投得区域 2 非城投, 取得是非城投的区域
     */
    @Column
    private Long districtUniCode;
    /**
     * 区间名称
     */
    @Column
    private String districtName;
    /**
     * 一级行业编码
     */
    @Column
    private Long induLevel1Code;
    /**
     * 一级行业名称
     */
    @Column
    private String induLevel1Name;
    /**
     * 二级行业编码
     */
    @Column
    private Long induLevel2Code;
    /**
     * 二级行业名称
     */
    @Column
    private String induLevel2Name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreateTime() {
        return Objects.isNull(createTime) ? null : new Timestamp(createTime.getTime());
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = Objects.isNull(createTime) ? null : new Timestamp(createTime.getTime());
    }

    public Timestamp getUpdateTime() {
        return Objects.isNull(updateTime) ? null : new Timestamp(updateTime.getTime());
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = Objects.isNull(updateTime) ? null : new Timestamp(updateTime.getTime());
    }

    public Date getIssueDate() {
        return Objects.isNull(issueDate) ? null : new Date(issueDate.getTime());
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = Objects.isNull(issueDate) ? null : new Date(issueDate.getTime());
    }

    public Date getIssueDateMonday() {
        return Objects.isNull(issueDateMonday) ? null : new Date(issueDateMonday.getTime());
    }

    public void setIssueDateMonday(Date issueDateMonday) {
        this.issueDateMonday = Objects.isNull(issueDateMonday) ? null : new Date(issueDateMonday.getTime());
    }

    public Integer getIssueDateMonth() {
        return issueDateMonth;
    }

    public void setIssueDateMonth(Integer issueDateMonth) {
        this.issueDateMonth = issueDateMonth;
    }

    public Integer getIssueDateQuarter() {
        return issueDateQuarter;
    }

    public void setIssueDateQuarter(Integer issueDateQuarter) {
        this.issueDateQuarter = issueDateQuarter;
    }

    public Integer getIssueDateYear() {
        return issueDateYear;
    }

    public void setIssueDateYear(Integer issueDateYear) {
        this.issueDateYear = issueDateYear;
    }

    public Date getMaturityDate() {
        return Objects.isNull(maturityDate) ? null : new Date(maturityDate.getTime());
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = Objects.isNull(maturityDate) ? null : new Date(maturityDate.getTime());
    }

    public Date getMaturityDateMonday() {
        return Objects.isNull(maturityDateMonday) ? null : new Date(maturityDateMonday.getTime());
    }

    public void setMaturityDateMonday(Date maturityDateMonday) {
        this.maturityDateMonday = Objects.isNull(maturityDateMonday) ? null : new Date(maturityDateMonday.getTime());
    }

    public Integer getMaturityDateMonth() {
        return maturityDateMonth;
    }

    public void setMaturityDateMonth(Integer maturityDateMonth) {
        this.maturityDateMonth = maturityDateMonth;
    }

    public Integer getMaturityDateQuarter() {
        return maturityDateQuarter;
    }

    public void setMaturityDateQuarter(Integer maturityDateQuarter) {
        this.maturityDateQuarter = maturityDateQuarter;
    }

    public Integer getMaturityDateYear() {
        return maturityDateYear;
    }

    public void setMaturityDateYear(Integer maturityDateYear) {
        this.maturityDateYear = maturityDateYear;
    }

    public Long getBondUniCode() {
        return bondUniCode;
    }

    public void setBondUniCode(Long bondUniCode) {
        this.bondUniCode = bondUniCode;
    }

    public String getBondShortName() {
        return bondShortName;
    }

    public void setBondShortName(String bondShortName) {
        this.bondShortName = bondShortName;
    }

    public Long getComUniCode() {
        return comUniCode;
    }

    public void setComUniCode(Long comUniCode) {
        this.comUniCode = comUniCode;
    }

    public String getComFullName() {
        return comFullName;
    }

    public void setComFullName(String comFullName) {
        this.comFullName = comFullName;
    }

    public Integer getBondType() {
        return bondType;
    }

    public void setBondType(Integer bondType) {
        this.bondType = bondType;
    }

    public Integer getSecondMarket() {
        return secondMarket;
    }

    public void setSecondMarket(Integer secondMarket) {
        this.secondMarket = secondMarket;
    }

    public Integer getPublicOffering() {
        return publicOffering;
    }

    public void setPublicOffering(Integer publicOffering) {
        this.publicOffering = publicOffering;
    }

    public Integer getEmbeddedOption() {
        return embeddedOption;
    }

    public void setEmbeddedOption(Integer embeddedOption) {
        this.embeddedOption = embeddedOption;
    }

    public Integer getCouponRateType() {
        return couponRateType;
    }

    public void setCouponRateType(Integer couponRateType) {
        this.couponRateType = couponRateType;
    }

    public Integer getBondTenorDay() {
        return bondTenorDay;
    }

    public void setBondTenorDay(Integer bondTenorDay) {
        this.bondTenorDay = bondTenorDay;
    }

    public Integer getCrossMarketStatus() {
        return crossMarketStatus;
    }

    public void setCrossMarketStatus(Integer crossMarketStatus) {
        this.crossMarketStatus = crossMarketStatus;
    }

    public Integer getComExtRatingMapping() {
        return comExtRatingMapping;
    }

    public void setComExtRatingMapping(Integer comExtRatingMapping) {
        this.comExtRatingMapping = comExtRatingMapping;
    }

    public Integer getBondExtRatingMapping() {
        return bondExtRatingMapping;
    }

    public void setBondExtRatingMapping(Integer bondExtRatingMapping) {
        this.bondExtRatingMapping = bondExtRatingMapping;
    }

    public Integer getComListedStatus() {
        return comListedStatus;
    }

    public void setComListedStatus(Integer comListedStatus) {
        this.comListedStatus = comListedStatus;
    }

    public Integer getBusinessNature() {
        return businessNature;
    }

    public void setBusinessNature(Integer businessNature) {
        this.businessNature = businessNature;
    }

    public Integer getUdicStatus() {
        return udicStatus;
    }

    public void setUdicStatus(Integer udicStatus) {
        this.udicStatus = udicStatus;
    }

    public BigDecimal getIssueCouponRate() {
        return issueCouponRate;
    }

    public void setIssueCouponRate(BigDecimal issueCouponRate) {
        this.issueCouponRate = issueCouponRate;
    }

    public Integer getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(Integer issueStatus) {
        this.issueStatus = issueStatus;
    }

    public BigDecimal getActualIssueAmount() {
        return actualIssueAmount;
    }

    public void setActualIssueAmount(BigDecimal actualIssueAmount) {
        this.actualIssueAmount = actualIssueAmount;
    }

    public BigDecimal getIssueCoupon() {
        return issueCoupon;
    }

    public void setIssueCoupon(BigDecimal issueCoupon) {
        this.issueCoupon = issueCoupon;
    }

    public Long getProvinceUniCode() {
        return provinceUniCode;
    }

    public void setProvinceUniCode(Long provinceUniCode) {
        this.provinceUniCode = provinceUniCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Long getCityUniCode() {
        return cityUniCode;
    }

    public void setCityUniCode(Long cityUniCode) {
        this.cityUniCode = cityUniCode;
    }

    public Long getCityName() {
        return cityName;
    }

    public void setCityName(Long cityName) {
        this.cityName = cityName;
    }

    public Long getDistrictUniCode() {
        return districtUniCode;
    }

    public void setDistrictUniCode(Long districtUniCode) {
        this.districtUniCode = districtUniCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Long getInduLevel1Code() {
        return induLevel1Code;
    }

    public void setInduLevel1Code(Long induLevel1Code) {
        this.induLevel1Code = induLevel1Code;
    }

    public String getInduLevel1Name() {
        return induLevel1Name;
    }

    public void setInduLevel1Name(String induLevel1Name) {
        this.induLevel1Name = induLevel1Name;
    }

    public Long getInduLevel2Code() {
        return induLevel2Code;
    }

    public void setInduLevel2Code(Long induLevel2Code) {
        this.induLevel2Code = induLevel2Code;
    }

    public String getInduLevel2Name() {
        return induLevel2Name;
    }

    public void setInduLevel2Name(String induLevel2Name) {
        this.induLevel2Name = induLevel2Name;
    }
}