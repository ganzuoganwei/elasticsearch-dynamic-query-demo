package com.example.elasticserachdynamicquerydemo;

import java.math.BigDecimal;

/**
 * 描述
 *
 * @author xionglei
 * @date 2022/8/3 16:35
 **/
public class Test {

    public static void main(String[] args) {
        double d = 325619.571715536d;
        BigDecimal bigDecimal = BigDecimal.valueOf(d);
        System.out.println(bigDecimal);
    }

}
