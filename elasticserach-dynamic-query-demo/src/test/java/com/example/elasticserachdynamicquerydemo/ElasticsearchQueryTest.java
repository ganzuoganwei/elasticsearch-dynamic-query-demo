package com.example.elasticserachdynamicquerydemo;

import com.example.elasticserachdynamicquerydemo.model.es.BondPrimaryStatsV2ES;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ParsedSum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@SpringBootTest
class ElasticsearchQueryTest {

    @Resource
    private ElasticsearchOperations elasticsearchOperations;

    @Test
    void listBondPrimaryStatsV2ES() {
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(new TermQueryBuilder("issueDateMonth", 201503)).build();
        SearchHits<BondPrimaryStatsV2ES> searchHits = elasticsearchOperations.search(query, BondPrimaryStatsV2ES.class);
        Assertions.assertEquals(searchHits.getSearchHits().size(), 1085);
    }

    @Test
    void testProvinceAmtSum() {
        TermsAggregationBuilder termsAggregationBuilder =
                AggregationBuilders.terms("provinceBucket").field("provinceUniCode").size(100)
                        .subAggregation(AggregationBuilders.sum("actualAmtSum").field("actualIssueAmount"));
        NativeSearchQueryBuilder query =
                new NativeSearchQueryBuilder().addAggregation(termsAggregationBuilder).withPageable(Pageable.ofSize(1));
        SearchHits<BondPrimaryStatsV2ES> search = elasticsearchOperations.search(query.build(), BondPrimaryStatsV2ES.class);
        System.out.println(search.getSearchHits().size());
        Map<String, Aggregation> stringAggregationMap = search.getAggregations().asMap();
        ParsedLongTerms terms = (ParsedLongTerms) stringAggregationMap.get("provinceBucket");
        List<? extends Terms.Bucket> buckets = terms.getBuckets();
        System.out.println(buckets.size());
        for (Terms.Bucket bucket : buckets) {
            System.out.println("省份编码: " + bucket.getKeyAsString());
            long docCount = bucket.getDocCount();
            System.out.println(docCount);
            Aggregations aggregations = bucket.getAggregations();
            ParsedSum actualAmtSum = aggregations.get("actualAmtSum");
            BigDecimal actualAmtSumValue = BigDecimal.valueOf(actualAmtSum.getValue());
            System.out.println(actualAmtSumValue);
        }
    }
}